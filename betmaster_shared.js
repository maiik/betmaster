var ix = Webflow.require('ix');


var ixBet = { "stepsA": [{ "transition": "transform 150ms ease-in-out 0ms", "x": "0px", "y": "-4px", "z": "0px" }], "stepsB": [] };
var ixUnBet = { "stepsA": [{ "transition": "transform 150ms ease-in-out 0ms", "x": "0px", "y": "0px", "z": "0px" }], "stepsB": [] };
var icBack = document.querySelector('.ic_back');
var ixBackLight = {'stepsA': [
  {'background': 'rgba(247, 247, 247, 1.0)', 'transition': 'background 600ms ease 0ms'}
], 'stepsB': []};
var ixMoveDown = { "stepsA": [{ "transition": "transform 150ms ease-in-out 0ms", "x": "0px", "y": "100px", "z": "0px" }], "stepsB": [] };
var ixMoveUp = { "stepsA": [{ "transition": "transform 150ms ease-in-out 0ms", "x": "0px", "y": "-100px", "z": "0px" }], "stepsB": [] };
var ixThumbMove = { "stepsA": [{ "transition": "transform 150ms ease-in-out 0ms", "x": "-30px", "y": "0px", "z": "0px" }], "stepsB": [] };

var ixShowOpacity = {'stepsA': [{'opacity': 0}, {'opacity': 1, 'transition': 'opacity 500ms ease 0ms'}], 'stepsB': []};
var ixHideOpacity = { 'stepsA': [{ 'opacity': 1 }, { 'opacity': 0, 'transition': 'opacity 500ms ease 0ms' }], 'stepsB': [] };
var ixHideOpacityLong = {'stepsA': [{'opacity': 1}, {'opacity': 0, 'transition': 'opacity 800ms ease 0ms'}], 'stepsB': []};
var ixHideOpacityFast = { 'stepsA': [{ 'opacity': 1 }, { 'opacity': 0, 'transition': 'opacity 20ms ease 0ms' }], 'stepsB': [] };

var menuToLeft = {'stepsA': [{"transition":"transform 500ms ease 0ms","x":"-380px","y":"0px","z":"0px"}], 'stepsB': []};


/**
 * Applies function to elements found by selector
 * @param {string} selectorName selector (class name or id) i. e. '.name'
 * @param {Function} what to do with collection
 */
function collectionDo(selectorName, doThis) {
  var arr;
  if (typeof (selectorName) === 'object') {
    arr = Array.prototype.slice.call(selectorName);
  }
  if (typeof (selectorName) === 'string') {
    arr = Array.prototype.slice.call(document.querySelectorAll(selectorName));
  }
  arr.forEach(function(el) {
    doThis(el);
  })
}


/**
 * Rounds the number
 * @param {number} num number to round
 * @param {boolean=} yes true = round anyway
 */
function roundOrNot(num, yes) {
  if (num >= 100 || yes) {
    return Number(Math.round(num)) || 0;
  } else {
    return Number(Math.round((num) * 100) / 100) || 0;
  }
}



function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}



var leagues = [
  'Премьер-лига',
  'Португалия - Примейра',
  'Кубок Португалии',
  'Нидерланды - Эредивизия',
  'Кубок Нидерландов',
  'MLS - Высшая лига футбола',
  'Турция - Суперлига',
  'Бельгия - Лига Жюпиле',
  'Австралия - А-лига',
  'Австрия - Бундеслига',
  'Азербайджан - Премьер-лига',
  'Армения - Премьер-лига',
  'Болгария - Группа А',
  'Венгрия - OTP Банк Лига',
  'Греция - Суперлига',
  'Дания - Суперлига',
  'Китай - Суперлига',
  'Польша - Экстракласса',
  'Румыния - Лига 1',
  'Сербия - Суперлига',
  'Словакия - Суперлига',
  'Узбекистан - ПФЛ',
  'Хорватия - Первая лига',
  'Чехия - Высшая лига',
  'Швейцария - Суперлига',
  'Шотландия - Премьер-лига',
  'Россия - Премьер-лига КФС',
  'Товарищеские матчи (сборные)',
  'Товарищеские матчи (клубы)',
  'ЧЕ-2016 - футзал'
];


var leaguesFlags = [
  'flag-ad',
  'flag-ae',
  'flag-af',
  'flag-ag',
  'flag-ai',
  'flag-al',
  'flag-am',
  'flag-an',
  'flag-ao',
  'flag-ar',
  'flag-as',
  'flag-at',
  'flag-au',
  'flag-aw',
  'flag-az',
  'flag-ba',
  'flag-bb',
  'flag-bd',
  'flag-be',
  'flag-bf',
  'flag-bg',
  'flag-bh',
  'flag-bi',
  'flag-bj',
  'flag-bm',
  'flag-bn'
]

var teams = {
  'BCN': {
    'name': 'FC&nbsp;Barcelona',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56ba52e22cc69d4e6918cfdf_0.png',
    'logobl': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56ba52f00cccd23909606c1b_0.png',
    'league': 'La Liga'
  },
  'US': {
    'name': 'FC USA Soccer',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b098707fb67c280d6b75e9_500px-US_Soccer_logo.png',
    'logobl': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56ba52f00cccd23909606c1c_500px-US_Soccer_logo.png',
    'league': 'US Soccer League'
  },
  'Brn': {
    'name': 'FC Bayern Munich',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b0987097187ad02c117974_FC_Bayern_0.png',
    'logobl': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56ba574c0cccd239096075a5_FC_Bayern_0.png',
    'league': 'Бундеслига'
  },
  'Shht': {
    'name': 'Караганда Шахтер',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b09870666c49ce174f6939_Shakhtyor-Karaganda.png',
    'logobl': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56ba52f02cc69d4e6918cfe1_Shakhtyor-Karaganda.png',
    'league': 'Kazakhstan Premier League'
  },
  'Ksc': {
    'name': 'FC&nbsp;VSS Košice',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b09870666c49ce174f693a_MFK-Kosice.png',
    'logobl': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56ba574b590c6fab210ac6f7_MFK-Kosice.png',
    'league': '2. Liga'
  },
  'Ukr': {
    'name': 'ФК&nbsp;Украина',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b098707fb67c280d6b75ea_Ukraine.png',
    'logobl': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56ba52f02cc69d4e6918cfe0_Ukraine.png',
    'league': 'Украинская Лига'
  },
  'Manch': {
    'name': 'Manchester United',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b098707fb67c280d6b75eb_manchester-united.png ',
    'logobl': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56ba52f00cccd23909606c1b_0.png',
    'league': 'Premier League, FA Cup'
  },
  'Mex': {
    'name': 'Mexicana FC',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b09871835a1cc917f6a339_e70c446871b9e1372bde9cb3fae27c65.png',
    'logobl': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56ba52f023ac054969b8909a_e70c446871b9e1372bde9cb3fae27c65.png',
    'league': 'Mexican League'
  },
  'Spn': {
    'name': 'Spain National FC',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b0987197187ad02c117975_Spanish-logo.png',
    'logobl': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56ba52f00cccd23909606c1e_Spanish-logo.png',
    'league': 'La Liga'
  },
  'Mad': {
    'name': 'Real Madrid C.F.',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56ddf5c2d28ecefc22b3b71c_real.jpg',
    'logobl': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56ddf5bbdb756cf23ebb4c4e_mad.jpg',
    'league': 'La Liga'
  }
}


var players = {
  'bcn_jordi': {
    'team': 'BCN',
    'name': 'Жорди Масип',
    'photo': 'http://s5o.ru/storage/simple/ru/edt/62/80/81/34/rue97efb6d653.jpeg',
    'type': 'Вратарь'
  },
  'bcn_claud': {
    'team': 'BCN',
    'name': 'Клаудио Браво',
    'photo': 'http://s5o.ru/storage/simple/ru/edt/10/20/78/23/rued24e422950.jpeg',
    'type': 'Вратарь'
  },
  'bcn_piq': {
    'team': 'BCN',
    'name': 'Жерар Пике',
    'photo': 'http://s5o.ru/storage/simple/ru/edt/22/79/46/41/rue574330eecc.jpeg',
    'type': 'Защитник'
  },
  'bcn_ney': {
    'team': 'BCN',
    'name': 'Неймар',
    'photo': 'http://s5o.ru/storage/simple/ru/edt/48/44/49/52/rue2c22d09554.jpeg',
    'type': 'Нападающий'
  }
}

var bets = {
  'bet1': {
    'name': 'Кто победит в основное время'
  },
  'bet2': {
    'name': 'Кто победит в доп. время'
  },
  'bet3': {
    'name': 'Кто победит по пенальти'
  }
}


var data = [
  {
    'type': 'bet',
    'team1': 'BCN',
    'team2': 'Brn',
    'result': '',
    'Score': '3 – 2',
    'bet': 'bet1',
    'payed': '20',
    'q': '1.4'
  },
  {
    'type': 'bet',
    'team1': 'Brn',
    'team2': 'Ksc',
    'result': '',
    'Score': '3 – 4',
    'bet': 'bet2',
    'payed': '10',
    'q': '2.4'
  },
  {
    'type': 'bet',
    'team1': 'Manch',
    'team2': 'Mex',
    'result': '',
    'Score': '0:0',
    'bet': 'bet3',
    'payed': '30',
    'q': '1.57'
  },
  {
    'type': 'bet',
    'team1': 'Mad',
    'team2': 'BCN',
    'result': 'adv',
    'Score': '0:0',
    'bet': 'bet3',
    'payed': '12',
    'q': '1.2',
    'banner': true,
    'bannerTitle': 'Эль Классико',
    'bannerUrl': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56de06e502e26bed3e5a3d9e_main_banner_bg1.png'
  },
  {
    'type': 'bet',
    'team1': 'Brn',
    'team2': 'Mex',
    'result': '',
    'Score': '0:0',
    'bet': 'bet3',
    'payed': '65',
    'q': '1.2'
  },
  {
    'type': 'day',
    'title': 'Завтра, 5 февраля'
  },
  {
    'type': 'bet',
    'team1': 'Ksc',
    'team2': 'Mex',
    'result': 'adv',
    'Score': '0:0',
    'bet': 'bet3',
    'payed': '50',
    'q': '1.1'
  },
  {
    'type': 'bet',
    'team1': 'Brn',
    'team2': 'BCN',
    'result': 'adv',
    'Score': '0:0',
    'bet': 'bet3',
    'payed': '40',
    'q': '1.5'
  },
  {
    'type': 'bet',
    'team1': 'BCN',
    'team2': 'Brn',
    'result': '',
    'Score': '3:2',
    'bet': 'bet1',
    'payed': '20',
    'q': '1.4'
  },
  {
    'type': 'bet',
    'team1': 'Brn',
    'team2': 'Ksc',
    'result': 'win',
    'Score': '3:4',
    'bet': 'bet2',
    'payed': '10',
    'q': '1.4'
  },
  {
    'type': 'day',
    'title': '6 февраля'
  },
  {
    'type': 'bet',
    'team1': 'Ukr',
    'team2': 'Manch',
    'result': 'lose',
    'Score': '1:0',
    'bet': 'bet3',
    'payed': '15',
    'q': '1.4'
  },
  {
    'type': 'bet',
    'team1': 'Manch',
    'team2': 'Mex',
    'result': 'win',
    'Score': '0:0',
    'bet': 'bet3',
    'payed': '17',
    'q': '1.4'
  },
  {
    'type': 'day',
    'title': 'Прошедшее'
  }
]

var coef = [1.33, 1.11, 1.56, 2.21, 5.30, 2.10, 4.39, 1.74, 1.33, 1.11, 1.56, 2.21, 5.30, 2.10, 4.39, 1.74, 1.33, 1.11, 1.56, 2.21, 5.30, 2.10, 4.39, 1.74, 1.33, 1.11, 1.56, 2.21, 5.30, 2.10, 4.39, 1.74, 1.33, 1.11, 1.56, 2.21, 5.30, 2.10, 4.39, 1.74, 1.33, 1.11, 1.56, 2.21, 5.30, 2.10, 4.39, 1.74]


var times = {
  'time50': 'url(\'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/572232ca5f1003c70fe0c443_mtime_2.svg\')',
  'time40': 'url(\'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/572232ca5f1003c70fe0c442_mtime_0.svg\')',
  'time30': 'url(\'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/572232ca5f1003c70fe0c444_mtime_3.svg\')',
  'time20': 'url(\'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/572232ca719e68556356f059_mtime_1.svg\')',
  'time10': 'url(\'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/572232cabc894a0c22a6be42_mtime_4.svg\')'
}


var dataG = [
  {
    'type': 'group',
    'title': leagues[0],
    'flag': leaguesFlags[0]
  },
  {
    'type': 'bet',
    'team1': 'BCN',
    'team2': 'Brn',
    'result': '',
    'Score': '3 – 2',
    'bet': 'bet1',
    'payed': '20',
    'q': '1.4',
    'time': '00:21:23',
    'time2': 'Через 2 ч. 30 мин.',
    'TimeBg': times['time10']
  },
  {
    'type': 'bet',
    'team1': 'Brn',
    'team2': 'Ksc',
    'result': '',
    'bet': 'bet2',
    'payed': '10',
    'q': '2.4',
    'time': '12',
    'time2': 'марта'
  },
  {
    'type': 'group',
    'title': leagues[5],
    'flag': leaguesFlags[5]
  },
  {
    'type': 'bet',
    'team1': 'Manch',
    'team2': 'Mex',
    'result': '',
    'Score': '0 – 0',
    'bet': 'bet3',
    'payed': '30',
    'q': '1.57',
    'time': '00:34:41',
    'time2': 'Через 2 ч. 50 мин.',
    'TimeBg': times['time20']
  },
  {
    'type': 'bet',
    'team1': 'Brn',
    'team2': 'Mex',
    'Score': '3 – 4',
    'result': '',
    'bet': 'bet3',
    'payed': '65',
    'q': '1.2',
    'time': '13:01:12',
    'time2': 'Через 3 ч.',
    'TimeBg': times['time30']
  },
  {
    'type': 'group',
    'title': 'Эль Классико!',
    'flag': leaguesFlags[5],
    // 'banner': true,
    'bannerTitle': 'Эль Классико',
    'bannerUrl': 'http://img.bleacherreport.net/img/images/photos/003/546/730/hi-res-69e08d04f6a5435934775c11db57c8ea_crop_north.jpg?w=630&h=420&q=75',
  },
  {
    'type': 'bet',
    'team1': 'Mad',
    'team2': 'BCN',
    'result': 'adv',
    'bet': 'bet3',
    'payed': '12',
    'q': '1.2',
    'time': '13:00',
    'time2': 'Через 3 ч.'
  },
  {
    'type': 'group',
    'title': leagues[1],
    'flag': leaguesFlags[1]
  },
  {
    'type': 'bet',
    'team1': 'Ksc',
    'team2': 'Mex',
    'result': 'adv',
    'bet': 'bet3',
    'payed': '50',
    'q': '1.1',
    'time': '12:30',
    'time2': 'Через 2 ч. 30 мин.',
  },
  {
    'type': 'bet',
    'team1': 'Brn',
    'team2': 'BCN',
    'result': 'adv',
    'Score': '0 – 0',
    'bet': 'bet3',
    'payed': '40',
    'q': '1.5',
    'time': '00:14:12',
    'time2': 'Через 2 ч. 30 мин.',
    'TimeBg': times['time40']
  },
  {
    'type': 'bet',
    'team1': 'BCN',
    'team2': 'Brn',
    'result': '',
    'bet': 'bet1',
    'payed': '20',
    'q': '1.4',
    'time': '12:30',
    'time2': 'Через 2 ч. 30 мин.'
  },
  {
    'type': 'bet',
    'team1': 'Brn',
    'team2': 'Ksc',
    'result': 'win',
    'Score': '0 – 0',
    'bet': 'bet2',
    'payed': '10',
    'q': '1.4',
    'time': '67:14:23',
    'time2': 'Через 2 ч. 30 мин.',
    'TimeBg': times['time50']
  },
  {
    'type': 'group',
    'title': leagues[3],
    'flag': leaguesFlags[3]
  },
  {
    'type': 'bet',
    'team1': 'Ukr',
    'team2': 'Manch',
    'result': 'lose',
    'bet': 'bet3',
    'payed': '15',
    'q': '1.4',
    'time': '12:30',
    'time2': 'Через 2 ч. 30 мин.'
  },
  {
    'type': 'bet',
    'team1': 'Manch',
    'team2': 'Mex',
    'result': 'win',
    'bet': 'bet3',
    'payed': '17',
    'q': '1.4',
    'time': '12:30',
    'time2': 'Через 2 ч. 30 мин.'
  }
]


function updateVals(value) {
  var newNum = [];
  // var arr1 = Array.prototype.slice.call(price1);
  for (var i = 0; i < coef.length; i++) {

    newNum[i] = roundOrNot(value * coef[i], true) - value;

    collectionDo('.bet_sum_dynamic', function(el) {
      el.innerHTML = value;
    })
    collectionDo('.bet_bonus_dynamic', function(el) {
      el.innerHTML = value * 0.1;
    })

    collectionDo('.dynamic_price', function(el) {
      el.innerHTML = newNum[getRandomInt(0, newNum.length - 1)] + '&thinsp;₽';
    })

      // el.innerHTML = '+&thinsp;' + newNum + '&thinsp;₽';

      // el0.innerHTML = value + '&thinsp;₽';
      // var el1 = arr1[i];
      // el1.innerHTML = roundOrNot(value * coef[i], true)  + '&thinsp;₽';

  }
}





//**************** RIPPLE *****************//
var addRippleEffect = function (e) {
  var target = e.target;
  if (target.parentNode.classList.contains('clck')) {
    target = target.parentNode;
  }
  if (target.parentNode.parentNode.classList.contains('clck')) {
    target = target.parentNode.parentNode;
  }
  // if (target.parentNode.parentNode.parentNode.classList.contains('clck')) {
  //   target = target.parentNode.parentNode.parentNode;
  // }
  if (!target.classList.contains('clck')) return false;

  var rect = target.getBoundingClientRect();
  var ripple = target.querySelector('.ripple');
  if (!ripple) {
    ripple = document.createElement('span');
    ripple.className = 'ripple';
    ripple.style.height = ripple.style.width = Math.max(rect.width, rect.height) + 'px';
    target.appendChild(ripple);
  }
  ripple.classList.remove('show');
  var top = e.pageY - rect.top - ripple.offsetHeight / 2 - document.body.scrollTop;
  var left = e.pageX - rect.left - ripple.offsetWidth / 2 - document.body.scrollLeft;
  ripple.style.top = top + 'px';
  ripple.style.left = left + 'px';
  ripple.classList.add('show');
  return false;
}
document.addEventListener('click', addRippleEffect, false);



// URLS
var fromUrl = window.location.search.substring(1);
var teamsFromUrl;
var one;
var two;
var manyBet;
var betCheck = document.querySelector('.match_top_bet');
var betCheckNum = betCheck.querySelector('.match_top_bet_num');

(function() {

  // URLS and Bets
  if (fromUrl) {
    manyBet = Number(fromUrl.split('&')[1].split('=')[1]);
    teamsFromUrl = fromUrl.split('&')[0].split('=')[1].split(',');
    one = teamsFromUrl[0];
    two = teamsFromUrl[1];
  } else {
    manyBet = 0;
    teamsFromUrl = '';
  }

  if(manyBet) {
    betCheck.classList.remove('hidden');
    betCheckNum.innerHTML = manyBet;
  } else {
    betCheck.classList.add('hidden');
  }

})();




var target = window; // this can be any scrollable element
var last_y = 0;
target.addEventListener('touchmove', function(e){
    var scrolly = target.pageYOffset || target.scrollTop || 0;
    var direction = e.changedTouches[0].pageY > last_y ? 1 : -1;
    if(direction>0 && scrolly===0){
        e.preventDefault();
    }
    last_y = e.changedTouches[0].pageY;
});





$('#reload').click(function() {
  location: reload(true);
});