var teams = {

  'BCN': {
    'name': 'Barcelona',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b1c72ea29233ce7a34a1ba_0.jpg'
  },
  'US': {
    'name': 'US',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b098707fb67c280d6b75e9_500px-US_Soccer_logo.png'
  },
  'Brn': {
    'name': 'Bayern',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b0987097187ad02c117974_FC_Bayern_0.png'
  },
  'Shht': {
    'name': 'Шахтар',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b09870666c49ce174f6939_Shakhtyor-Karaganda.png'
  },
  'Ksc': {
    'name': 'Kosice',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b09870666c49ce174f693a_MFK-Kosice.png'
  },
  'Ukr': {
    'name': 'Украина',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b098707fb67c280d6b75ea_Ukraine.png'
  },
  'Manch': {
    'name': 'Manchester',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b098707fb67c280d6b75eb_manchester-united.png '
  },
  'Mex': {
    'name': 'Mexicana',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b09871835a1cc917f6a339_e70c446871b9e1372bde9cb3fae27c65.png'
  },
  'Spn': {
    'name': 'Spanish',
    'logo': 'https://daks2k3a4ib2z.cloudfront.net/56b073c4666c49ce174f499c/56b0987197187ad02c117975_Spanish-logo.png'
  }
}


var bets = {
  'bet1': {
    'name': 'Кто победит в основное время'
  },
  'bet2': {
    'name': 'Кто победит в доп. время'
  },
  'bet3': {
    'name': 'Кто победит по пенальти'
  }
}


var data = [
  {
    'type': 'bet',
    'team1': 'BCN',
    'team2': 'Brn',
    'result': '',
    'Score': '3:2',
    'bet': 'bet1',
    'payed': '20',
    'q': '1.4'
  },
  {
    'type': 'bet',
    'team1': 'Brn',
    'team2': 'Ksc',
    'result': '',
    'Score': '3:4',
    'bet': 'bet2',
    'payed': '10',
    'q': '2.4'
  },
  {
    'type': 'day',
    'title': '6 февраля'
  },
  {
    'type': 'bet',
    'team1': 'Manch',
    'team2': 'Mex',
    'result': '',
    'Score': '0:0',
    'bet': 'bet3',
    'payed': '30',
    'q': '1.57'
  },
  {
    'type': 'bet',
    'team1': 'Ksc',
    'team2': 'Spn',
    'result': 'adv',
    'Score': '0:0',
    'bet': 'bet3',
    'payed': '12',
    'q': '1.2'
  },
  {
    'type': 'bet',
    'team1': 'Brn',
    'team2': 'Mex',
    'result': '',
    'Score': '0:0',
    'bet': 'bet3',
    'payed': '65',
    'q': '1.2'
  },
  {
    'type': 'day',
    'title': 'Завтра, 5 февраля'
  },
  {
    'type': 'bet',
    'team1': 'Ksc',
    'team2': 'Mex',
    'result': 'adv',
    'Score': '0:0',
    'bet': 'bet3',
    'payed': '50',
    'q': '1.1'
  },
  {
    'type': 'bet',
    'team1': 'Brn',
    'team2': 'BCN',
    'result': 'adv',
    'Score': '0:0',
    'bet': 'bet3',
    'payed': '40',
    'q': '1.5'
  },
  {
    'type': 'day',
    'title': 'Сегодня, 4 февраля'
  },
  {
    'type': 'bet',
    'team1': 'BCN',
    'team2': 'Brn',
    'result': '',
    'Score': '3:2',
    'bet': 'bet1',
    'payed': '20',
    'q': '1.4'
  },
  {
    'type': 'bet',
    'team1': 'Brn',
    'team2': 'Ksc',
    'result': 'win',
    'Score': '3:4',
    'bet': 'bet2',
    'payed': '10',
    'q': '1.4'
  },
  {
    'type': 'bet',
    'team1': 'Ukr',
    'team2': 'Manch',
    'result': 'lose',
    'Score': '1:0',
    'bet': 'bet3',
    'payed': '15',
    'q': '1.4'
  },
  {
    'type': 'bet',
    'team1': 'Manch',
    'team2': 'Mex',
    'result': 'win',
    'Score': '0:0',
    'bet': 'bet3',
    'payed': '17',
    'q': '1.4'
  },
  {
    'type': 'day',
    'title': 'Прошедшее'
  }
]

var ix = Webflow.require('ix');
var ixMoveIcUp = { 'stepsA': [{'transition': 'transform 10ms ease-out 0ms', 'x': '-700px', 'y': '0px', 'z': '0px' }], 'stepsB': [{}] };
var ixMoveIcDown;
function ixShow(i, element) {
  switch (i) {
    case 1:
      ixMoveIcDown = { 'stepsA': [{'transition': 'transform 400ms ease-out 0ms', 'x': '0px', 'y': '0px', 'z': '0px' }], 'stepsB': [{}] };
      break;
    case 2:
      ixMoveIcDown = { 'stepsA': [{'transition': 'transform 400ms ease-in-cubic 0ms', 'x': '0px', 'y': '0px', 'z': '0px' }], 'stepsB': [{}] };
      break;
  }
  ix.run(ixMoveIcDown, element);
}




function upAll(container, el) {
  var items1 = container.querySelectorAll(el);
  for (var i = 0; i < items1.length; i++) {
    var element = items1[i];
    ix.run(ixMoveIcUp, element);
  }
}


function showElements(container, el) {
  var items1 = container.querySelectorAll(el);
  // upAll(itemsContainer, '.m_one');
  var i = 0;
  function f() {
    var element = items1[i];
    if (element.id !== 'item_template') {
      element.classList.remove('hidden');
      ixShow(1, element);
    }
    i++;
    if (i < items1.length) {
      setTimeout(f, 20 + ((items1.length) / 20) * 60);
    }
  }
  f();
  if (i === items1.length - 1) {
    window.scrollTo(0,document.body.scrollHeight);
  }
  // containerH();
}

function hideElements(container, el) {
  var items1 = container.querySelectorAll(el);
  var i = items1.length;
  function f() {
    var element = items1[i];
    ixShow(2, element);
    i--;
    if (i >= 0) {
      setTimeout(f, 20 + (i / 20) * 20);
    }
  }
  f();
}


// item_template


function drawItemsType(container, el) {
  //delete default items
  var items1 = container.querySelectorAll(el);
  for (var i = 0; i < items1.length; i++) {
    var element = items1[i];
    if (element.id !== 'item_template' || element.id !== 'item_day_template') {
      element.parentNode.removeChild(element);
    }
  }
  //show default set
  for (var u = data.length - 1; u >= 0 ; u--) {
    var obj = data[u];
    // if (obj[type] === 'bet') {
      drawItem(obj, obj['type']);
    // }
  }
}



/**
 * Rounds the number
 * @param {number} num number to round
 * @param {boolean=} yes true = round anyway
 */
function roundOrNot(num, yes) {
  if (num >= 100 || yes) {
    return Number(Math.round(num)) || 0;
  } else {
    return Number(Math.round((num) * 100) / 100) || 0;
  }
}





var template = document.querySelector('#item_template');
var templateDay = document.querySelector('#item_day_template');

function drawItem(obj, type) {
  var item;
  if (type === 'bet') {
    item = template.cloneNode(true);
    item.id = '';
    // item.classList.remove('hidden');

    var team1 = teams[obj.team1];
    var team2 = teams[obj.team2];

    item.querySelector('.item_adv_add').classList.add('hidden');
    item.querySelector('.l_1').style.backgroundImage = 'url(' + team1.logo + ')';
    item.querySelector('.l_2').style.backgroundImage = 'url(' + team2.logo + ')';

    item.querySelector('.name_1').textContent = obj.team1;
    item.querySelector('.name_2').textContent = obj.team2;

    item.querySelector('.bet_name').textContent = bets[obj.bet]['name'];

    item.querySelector('.bet_0').style.width = obj.payed + '%';
    var priceEarn = roundOrNot((Number(obj.payed) * Number(obj.q)), true);

    item.querySelector('.bet_1').style.width = (priceEarn - obj.payed) + '%';
    item.querySelector('.bet_price_0').textContent = obj.payed + '₽';


    if (obj.result === 'adv') {
      item.querySelector('.item_adv_add').classList.remove('hidden');
      item.querySelector('.betdetails').classList.add('hidden');
      item.classList.add('adv');
    }

    if (obj.result === 'win') {
      item.querySelector('.bet_sc_0').classList.add('bet_win');
      item.querySelector('.bet_sc_1').classList.add('bet_win');
      item.querySelector('.bet_price_0').classList.add('bet_win');
      item.querySelector('.bet_price_1').classList.add('bet_win');
    }
    if (obj.result === 'lose') {
      item.querySelector('.bet_sc_0').classList.add('bet_lose');
      item.querySelector('.bet_sc_1').classList.add('bet_lose');
      item.querySelector('.bet_price_0').classList.add('bet_lose');
      item.querySelector('.bet_price_1').classList.add('bet_lose');
    }

    item.querySelector('.bet_price_1').textContent =  priceEarn + '₽';
  }

  if (type === 'day') {
    item = templateDay.cloneNode(true);
    item.id = '';
    if (obj.title === 'Прошедшее') {
      item.classList.add('day_past');
    }
    item.querySelector('.day_title').textContent = obj.title;
  }

  // itemsContainer.appendChild(item);
  itemsContainer.insertBefore(item, itemsContainer.childNodes[0]);
}


var itemsContainer = document.querySelector('.my');
var afisha = document.querySelector('.af');
var windowW = window.innerWidth;
var windowH = window.innerHeight;



var afishaOpen = false;
var searchCont = document.querySelector('.search_cont');
var fabMat = document.querySelector('.fab_material');

afisha.addEventListener('click', function (evt) {
  if (!evt.target.classList.contains('fab') && !evt.target.parentNode.classList.contains('fab')) {
    //afishaon
    setTimeout(function() {
      ix.run(ixShowMenu, afisha);
    }, 10);
    document.querySelector('.my').style.webkitFilter = 'blur(20px)';
    document.querySelector('.my_filters').classList.add('hidden');
    afishaOpen = true;
  } else {
    //searchon

  }
});


var ixSearchon = { 'stepsA': [{'opacity':1, 'transition':'transform 500ms ease 0ms, opacity 500ms ease 0ms', 'scaleX':5, 'scaleY':5, 'scaleZ':1}], 'stepsB': []}
var ixShowMenu = { 'stepsA': [{ 'transition': 'transform 300ms ease-out 0ms', 'x': '0px', 'y': -(windowH - 140), 'z': '0px' }], 'stepsB': [] };
var ixHideMenu = { 'stepsA': [{ 'transition': 'transform 300ms ease-out 0ms', 'x': '0px', 'y': 0, 'z': '0px' }], 'stepsB': [] };


//On resize
window.onresize = function () {
  windowW = window.innerWidth;
  windowH = window.innerHeight;
}


itemsContainer.addEventListener('click', function (evt) {
  if (afishaOpen) {
    ix.run(ixHideMenu, afisha);
    document.querySelector('.my').style.webkitFilter = 'blur(0px)';
    document.querySelector('.my_filters').classList.remove('hidden');
    afishaOpen = false;
  }
});



(function () {
  drawItemsType(itemsContainer, '.m_one');

  upAll(itemsContainer, '.m_one');
  setTimeout(function() {
    showElements(itemsContainer, '.m_one');
  }, 500);

  afisha.style.height = (windowH - 70) + 'px';
  setTimeout(function() {
    afisha.style.top = (windowH - 70) + 'px';
  }, 200);

})();