
var ixMoveIcUp = { 'stepsA': [{'transition': 'transform 10ms ease-out 0ms', 'x': '-700px', 'y': '0px', 'z': '0px' }], 'stepsB': [{}] };
var ixMoveIcDown;
function ixShow(i, element) {
  switch (i) {
    case 1:
      ixMoveIcDown = { 'stepsA': [{'transition': 'transform 400ms ease-out 0ms', 'x': '0px', 'y': '0px', 'z': '0px' }], 'stepsB': [{}] };
      break;
    case 2:
      ixMoveIcDown = { 'stepsA': [{'transition': 'transform 400ms ease-in-cubic 0ms', 'x': '0px', 'y': '0px', 'z': '0px' }], 'stepsB': [{}] };
      break;
  }
  ix.run(ixMoveIcDown, element);
}




function upAll(container, el) {
  var items1 = container.querySelectorAll(el);
  for (var i = 0; i < items1.length; i++) {
    var element = items1[i];
    ix.run(ixMoveIcUp, element);
  }
}


function showElements(container, el) {
  var items1 = container.querySelectorAll(el);
  // upAll(itemsContainer, '.m_one');
  var i = 0;
  function f() {
    var element = items1[i];
    if (element.id !== 'item_template') {
      element.classList.remove('hidden');
      ixShow(1, element);
    }
    i++;
    if (i < items1.length) {
      setTimeout(f, 20 + ((items1.length) / 20) * 60);
    }
  }
  f();
  if (i === items1.length - 1) {
    window.scrollTo(0,document.body.scrollHeight);
  }
  // containerH();
}

function hideElements(container, el) {
  var items1 = container.querySelectorAll(el);
  var i = items1.length;
  function f() {
    var element = items1[i];
    ixShow(2, element);
    i--;
    if (i >= 0) {
      setTimeout(f, 20 + (i / 20) * 20);
    }
  }
  f();
}


// item_template


function drawItemsType(container, el) {
  //delete default items
  var items1 = container.querySelectorAll(el);
  for (var i = 0; i < items1.length; i++) {
    var element = items1[i];
    if (element.id !== 'item_template' || element.id !== 'item_day_template') {
      element.parentNode.removeChild(element);
    }
  }
  //show default set
  for (var u = data.length - 1; u >= 0 ; u--) {
    var obj = data[u];
    // if (obj[type] === 'bet') {
      drawItem(obj, obj['type']);
    // }
  }
}








var template = document.querySelector('#item_template');
var templateDay = document.querySelector('#item_day_template');

function drawItem(obj, type) {
  var item;
  if (type === 'bet') {
    item = template.cloneNode(true);
    item.id = '';
    item.querySelector('.m_one_inner').classList.remove('hidden');

    var team1 = teams[obj.team1];
    var team2 = teams[obj.team2];

    // item.querySelector('.item_adv_add').classList.add('hidden');
    item.querySelector('.l_1').style.backgroundImage = 'url(' + team1.logo + ')';
    item.querySelector('.l_2').style.backgroundImage = 'url(' + team2.logo + ')';

    item.querySelector('.name_1').innerHTML = team1.name;
    item.querySelector('.name_2').innerHTML = team2.name;

    // item.querySelector('.bet_name').textContent = bets[obj.bet]['name'];

    // item.querySelector('.bet_0').style.width = obj.payed + '%';
    var priceEarn = roundOrNot((Number(obj.payed) * Number(obj.q)), true);

    //item.querySelector('.bet_1').style.width = (priceEarn - obj.payed) + '%';
    // item.querySelector('.bet_price_0').textContent = obj.payed + '₽';

    if (obj.banner) {
      item.querySelector('.main_banner_h').classList.remove('hidden');
      item.classList.add('banner');
      item.querySelector('.main_banner_h').textContent = obj.bannerTitle;
      item.style.backgroundImage = ' linear-gradient(180deg, rgba(0, 0, 0, .34), rgba(0, 0, 0, .34)), url(' + obj.bannerUrl + ')';
      // item.classList.add('adv');
    } else {
      item.style.background = 'none';
    }

    // if (obj.result === 'adv') {
    //   item.querySelector('.item_adv_add').classList.remove('hidden');
    //   item.querySelector('.betdetails').classList.add('hidden');
    //   item.classList.add('adv');
    // }

    // if (obj.result === 'win') {
    //   item.querySelector('.bet_sc_0').classList.add('bet_win');
    //   item.querySelector('.bet_sc_1').classList.add('bet_win');
    //   item.querySelector('.bet_price_0').classList.add('bet_win');
    //   item.querySelector('.bet_price_1').classList.add('bet_win');
    // }
    // if (obj.result === 'lose') {
    //   item.querySelector('.bet_sc_0').classList.add('bet_lose');
    //   item.querySelector('.bet_sc_1').classList.add('bet_lose');
    //   item.querySelector('.bet_price_0').classList.add('bet_lose');
    //   item.querySelector('.bet_price_1').classList.add('bet_lose');
    // }

    // item.querySelector('.bet_price_1').textContent =  priceEarn + '₽';
  }

  if (type === 'day') {
    item = templateDay.cloneNode(true);
    item.id = '';
    if (obj.title === 'Прошедшее') {
      item.classList.add('day_past');
    }
    item.querySelector('.day_title').textContent = obj.title;
  }

  // itemsContainer.appendChild(item);
  itemsContainer.insertBefore(item, itemsContainer.childNodes[0]);
}


var itemsContainer = document.querySelector('.my');
var afisha = document.querySelector('.af');
var windowW = window.innerWidth;
var windowH = window.innerHeight;



var afishaOpen = false;
var searchCont = document.querySelector('.search_cont');
var fabMat = document.querySelector('.fab_material');

// afisha.addEventListener('click', function (evt) {
//   if (!evt.target.classList.contains('fab') && !evt.target.parentNode.classList.contains('fab')) {
//     //afishaon
//     setTimeout(function() {
//       ix.run(ixShowMenu, afisha);
//     }, 10);
//     document.querySelector('.my').style.webkitFilter = 'blur(20px)';
//     document.querySelector('.my_filters').classList.add('hidden');
//     afishaOpen = true;
//   } else {
//     //searchon

//   }
// });


var ixSearchon = { 'stepsA': [{'opacity':1, 'transition':'transform 500ms ease 0ms, opacity 500ms ease 0ms', 'scaleX':5, 'scaleY':5, 'scaleZ':1}], 'stepsB': []}
var ixShowMenu = { 'stepsA': [{ 'transition': 'transform 300ms ease-out 0ms', 'x': '0px', 'y': -(windowH - 140), 'z': '0px' }], 'stepsB': [] };
var ixHideMenu = { 'stepsA': [{ 'transition': 'transform 300ms ease-out 0ms', 'x': '0px', 'y': 0, 'z': '0px' }], 'stepsB': [] };


//On resize
window.onresize = function () {
  windowW = window.innerWidth;
  windowH = window.innerHeight;
}


itemsContainer.addEventListener('click', function (evt) {
  if (afishaOpen) {
    ix.run(ixHideMenu, afisha);
    document.querySelector('.my').style.webkitFilter = 'blur(0px)';
    document.querySelector('.my_filters').classList.remove('hidden');
    afishaOpen = false;
  }
});



(function () {
  drawItemsType(itemsContainer, '.m_one');

  upAll(itemsContainer, '.m_one');
  setTimeout(function() {
    showElements(itemsContainer, '.m_one');
  }, 500);

  collectionDo('.item_league', function (el) {
    el.textContent = leagues[getRandomInt(0, leagues.length)];
  });

  // afisha.style.height = (windowH - 70) + 'px';
  // setTimeout(function() {
  //   afisha.style.top = (windowH - 70) + 'px';
  // }, 200);

})();