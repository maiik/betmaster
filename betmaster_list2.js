var ixMoveLeft = { "stepsA": [{ "transition": "transform 150ms ease-in-out 0ms", "x": "-400px", "y": "0px", "z": "0px" }], "stepsB": [] };
var ixMoveRight0 = { "stepsA": [{ "transition": "transform 10ms ease-in-out 0ms", "x": "400px", "y": "0px", "z": "0px" }], "stepsB": [] };
var ixMove0 = { "stepsA": [{ "transition": "transform 150ms ease-in-out 0ms", "x": "0px", "y": "0px", "z": "0px" }], "stepsB": [] };

var pBets = document.querySelector('.p_bets');
var pMain = document.querySelector('.p_main');
var pBalance = document.querySelector('.p_balance');

var menuLeft = document.querySelector('.menu');
var menuZ = document.querySelector('.menu_z');
var menuBody = document.querySelector('.menu_body');


function hideMen() {
  ix.run(ixHideOpacity, menuZ);
  ix.run(menuToLeft, menuBody);
  setTimeout(function() {
    menuLeft.style.display = 'none';
  }, 500);
}


var mainContainer = document.querySelector('#content');


var template = document.querySelector('#item_template');
var templateGroup = document.querySelector('#container_template');
var templateNow = document.querySelector('#container_now');
var templateDay = document.querySelector('#item_day_template');

function drawMatch(obj, type) {
  var item;
  item = template.cloneNode(true);
  item.id = '';
  item.classList.remove('hidden');

  //add team logos and names
  var team1 = teams[obj.team1];
  var team2 = teams[obj.team2];
  item.querySelector('.l_1').style.backgroundImage = 'url(' + team1.logo + ')';
  item.querySelector('.l_2').style.backgroundImage = 'url(' + team2.logo + ')';
  item.querySelector('.name_1').innerHTML = team1.name;
  item.querySelector('.name_2').innerHTML = team2.name;

  if (obj.Score) {
    item.querySelector('.match_time').classList.add('score');
    item.querySelector('.match_time').innerHTML = obj.Score;
    item.querySelector('.match_date').innerHTML = obj.time;
    item.querySelector('.team_c').classList.add('team_c_goes');
    if (obj.TimeBg) {
      item.querySelector('.team_c').style['backgroundImage'] = obj.TimeBg;
    }
  } else {
    item.querySelector('.match_time').innerHTML = obj.time;
    item.querySelector('.match_date').innerHTML = obj.time2;
  }
  item.querySelector('.match_click').dataset.team1 = obj.team1;
  item.querySelector('.match_click').dataset.team2 = obj.team2;


  var lastCont = mainContainer.childNodes[mainContainer.childNodes.length - 1];
  lastCont.querySelector('.item_w').appendChild(item);
  // templateGroup.insertBefore(item, itemsContainer.childNodes[0]);

}


function drawGroup(obj) {
  var item;
  item = templateGroup.cloneNode(true);
  item.id = '';
  item.classList.remove('hidden');

  if (obj.banner) {
    item.classList.add('banner');
    item.style.backgroundImage = 'url(\'' + obj.bannerUrl + '\')';
  } else {
    item.style.backgroundImage = 'url(\'\')';
  }


  //add team logos and names
  item.querySelector('.item_league_name').innerHTML = obj['title'];
  item.querySelector('.item_league_icon').classList.add(obj['flag']);
  mainContainer.appendChild(item);
  // templateGroup.insertBefore(item, itemsContainer.childNodes[0]);

}

function drawNowSoon() {
  var item = document.querySelector('#container_now');
  item = templateNow.cloneNode(true);
  item.id = '';
  item.classList.remove('hidden');
  mainContainer.appendChild(item);
  // templateGroup.insertBefore(item, itemsContainer.childNodes[0]);
}


var mainType = 'typeLigas';
var oneGroup = true;

function drwItmsType(container, selctr) {

  // ix.run(ixHideOpacity, container);

  var items1 = container.querySelectorAll(selctr);

  //delete default items
  for (var i = 0; i < items1.length; i++) {
    var element = items1[i];
    if (!(element.id === 'container_template' || element.id === 'container_now')) {
      element.parentNode.removeChild(element);
    }
  }

  //show default set
  //if leagues
  if (mainType === 'typeNow' || mainType === 'typeSoon') {
    drawNowSoon();
  }
  //if leagues
  for (var u = 0; u < dataG.length ; u++) {
    var obj = dataG[u];
    if (obj['type'] === 'group' && mainType === 'typeLigas') {
      drawGroup(obj);
    }
    if (obj['type'] === 'bet') {
      switch (mainType) {
        case 'typeLigas':
          drawMatch(obj, obj['type']);
          break;
        case 'typeSoon':
          if (!obj['Score']) {
            drawMatch(obj, obj['type']);
          }
          break;
        case 'typeNow':
          if (obj['Score']) {
            drawMatch(obj, obj['type']);
          }
          break;
      }
    }
  }
  setTimeout(function() {
    // ix.run(ixShowOpacity, container);
  }, 0);


}


$('.s_main_filters').click(function() {
  var typ = this.dataset.type;
  console.log('typ = ', typ);
  mainType = typ;
  drwItmsType(mainContainer, '.m_one');
  if (mainType !== 'typeLigas') {
    oneGroup = false;
  }
  $('.s_main_filters').removeClass('active active_bottom');
  this.classList.add('active', 'active_bottom');
});





var searchInput = '<input type="text" class="srch srchinp" placeholder="Искать команды, лиги, игроков..." autofocus>';
var srch;
var srchCont = document.querySelector('.search');



(function() {

  drwItmsType(mainContainer, '.m_one');
  var srchInput = document.querySelector('.search_field');
  if (srchInput) {
    srchInput.innerHTML = searchInput;
    srch = document.querySelector('.srch');
    srch.focus();
  }


  //MAINPAGE
  if (document.body.classList.contains('body_main')) {
    document.querySelector('.top_search').addEventListener('click', function(evt) {
      setTimeout(function() {
        srch.focus();
      }, 500);
    });
    if (manyBet) {
      document.querySelector('.top_search').classList.add('moved');
    } else {
      document.querySelector('.top_search').classList.remove('moved');
    }

    document.querySelector('.bal_input_rubl').disabled;
    document.querySelector('.match_top_bet').addEventListener('click', function() {
      showPage('.p_bets');
      collectionDo('.menu_li', function(el) {
        el.classList.remove('active');
        el.querySelector('.menu_li_name').classList.remove('active');
        el.querySelector('.menu_li_icon').classList.remove('active');
      });
    })
    document.querySelector('#menu_bets_num').textContent = manyBet;


    srch.addEventListener('input', function(evt) {
      if (evt.target.value.length) {
        document.querySelector('.srch_placeholder').classList.add('hidden');
        srchCont.querySelector('.srch_preloader').classList.add('opacity');
        srchCont.querySelector('.srch_preloader').classList.remove('hidden');
        srchCont.querySelector('.srch_clear').classList.remove('hidden');
        setTimeout(function() {
          srchCont.querySelector('.p_scroll').classList.remove('hidden');
          srchCont.querySelector('.srch_preloader').classList.add('hidden');
          srchCont.querySelector('.srch_preloader').classList.remove('opacity');
        }, 800);
      } else {
        srchCont.querySelector('.srch_preloader').classList.add('hidden');
        srchCont.querySelector('.p_scroll').classList.add('hidden');
        document.querySelector('.srch_placeholder').classList.remove('hidden');
        srchCont.querySelector('.srch_clear').classList.add('hidden');
      }
    }, false);

    srchCont.querySelector('.srch_clear').addEventListener('click', function(evt) {
      srch.value = '';
      srchCont.querySelector('.srch_preloader').classList.add('hidden');
      srchCont.querySelector('.p_scroll').classList.add('hidden');
      document.querySelector('.srch_placeholder').classList.remove('hidden');
      srchCont.querySelector('.srch_clear').classList.add('hidden');
    });





  menuBody.addEventListener('click', function(evt) {
    var el = evt.target;
    if (!(el.classList.contains('menu_li') || el.parentNode.classList.contains('menu_li') || el.parentNode.parentNode.classList.contains('menu_li') || el.parentNode.parentNode.parentNode.classList.contains('menu_li'))) {
      return;
    }
    if (el.parentNode.classList.contains('menu_li')) {
      el = el.parentNode;
    }
    if (el.parentNode.parentNode.classList.contains('menu_li')) {
      el = el.parentNode.parentNode;
    }
    if (el.classList.contains('active')) {
      // hideMen();
      return;
    }

    collectionDo('.menu_li', function(el) {
      el.classList.remove('active');
      el.querySelector('.menu_li_name').classList.remove('active');
      el.querySelector('.menu_li_icon').classList.remove('active');
    });



    var item = el.dataset.menu;
    switch (item) {
      case 'main':
        console.log('main');
        showPage('.p_main');
        break;

      case 'bets':
        console.log('bets');
        showPage('.p_bets');
        break;

      case 'balance':
        console.log('balance');
        showPage('.p_balance');
        break;

      case 'history':
        console.log('history');
        showPage('.p_history');
        break;

      case 'settings':
        console.log('settings');
        // showPage('.p_settings');
        break;

      case 'call':
        console.log('call');
        showPage('.p_call');
        break;

      case 'license':
        console.log('license');
        showPage('.p_license');
        break;

      case 'logout':
        console.log('logout');
        // showPage('.p_logout');
        break;

      default:
        break;
    }
    el.classList.add('active');
    el.querySelector('.menu_li_name').classList.add('active');
    el.querySelector('.menu_li_icon').classList.add('active');

    // setTimeout(function() {
      // hideMen();
    // }, 300);
  });







  //MAINPAGE end
  } else {
    var balPopSum = document.querySelector('.bet_bal_baltext');
    balPopSum.querySelector('span').innerHTML = balance;

  }


})();





//Clicks
document.querySelector('.my').addEventListener('click', function(evt) {
  var el = evt.target;
  if (evt.target.classList.contains('match_click')) {
    one = el.dataset.team1;
    two = el.dataset.team2;
    window.location.href = "/match?tms=" + one + ',' + two + '&bets=' + manyBet;
  }
});
// srch.addEventListener('textInput', function(evt) {
// }, true);


// window.onload = function() {
//     WebPullToRefresh.init( {
//         loadingFunction: exampleLoadingFunction
//     } );
// };




function showPage(name) {
  collectionDo('.mainblock', function(el) {
    if (!el.classList.contains(name)) {
      el.classList.add('hidden');
    }
  });
  document.querySelector(name).classList.remove('hidden');
}


$('.my_container').on('click', '.item_league', function () {
  $('#menu_hamb').addClass('hidden');
  $('.topbar_sub').addClass('hidden');
  $('#top_back').removeClass('hidden');
  var leagName = this.querySelector('.item_league_name').textContent;
  $('#main_top_logo').text(leagName);
  mainType = 'typeSoon';
  drwItmsType(mainContainer, '.m_one');
});


$('#top_back').click(function () {
  $('#top_back').addClass('hidden');
  $('#menu_hamb').removeClass('hidden');
  $('.topbar_sub').removeClass('hidden');
  $('#main_top_logo').text('Betmaster');
  mainType = 'typeLigas';
  drwItmsType(mainContainer, '.m_one');
});

